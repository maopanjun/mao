Page({

  /**
   * 页面的初始数据
   */
  data: {
    'user': [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    var Util = require('../../utils/util.js');
    wx.request({
      url:'https://jsonplaceholder.typicode.com/todos', //必填，其他的都可以不填
      header: {
        'content-type': 'application/x-www-form-urlencoded' // POST用application/x-www-form-urlencoded
      },
      method: 'POST',
      //data: {userId:"1",id:"201",title:"week2",completed:"true"},
      data: Util.json2Form({userId:"1",id:"201",title:"week2",completed:"true"}),
      success (res) {
        that.setData( { 
          userId: res.data.userId,
          id: res.data.id,
          title: res.data.title,
          completed: res.data.completed,
         }); 
        console.log(res);
      },
      fail(){  
          console.log('failed');
      },
      complete(){   
          console.log('complete'); 
      }
      
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})