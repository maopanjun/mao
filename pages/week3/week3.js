// pages/week4/week4.js
import WxValidate from "../../utils/wxValidate";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    usernameErr: '',
    passwordErr: ''
  },
  
  usernameOnBlur(e) {
    var that = this;
    let username = e.detail.value;
    if (username == null || username == '') {
      that.setData({
      usernameErr: "Please enter username.",
    })
    return false;
    }else if (username.length>20){
      that.setData({
        usernameErr: "You can enter up to 20 characters.",
      })
      return false;
    }else{
      that.setData({
        usernameErr: "",
      })
      return true;
    }
  },

  passwordOnBlur(e) {
    var that = this;
    let password = e.detail.value;
    if (password == null || password == '') {
      that.setData({
        passwordErr: "Please enter password.",
    })
    return false;
    }else if (password.length>20){
      that.setData({
        passwordErr: "You can enter up to 20 characters.",
      })
      return false;
    }else{
      that.setData({
        passwordErr: "",
      })
      return true;
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
    // 验证字段的规则
    const rules = {
      username: {
          required: true,
          maxlength: 20,
      },
      password: {
          required: true,
          maxlength: 20,
      },
    }

    // 验证字段的提示信息，若不传则调用默认的信息
    const messages = {
      username: {
          required: 'Please enter username.',
          username: 'You can enter up to 20 characters.',
      },
      password: {
          required: 'Please enter password.',
          password: 'You can enter up to 20 characters.',
      },
}
// 创建实例对象
  this.WxValidate = new WxValidate(rules, messages)

  // 自定义验证规则
  this.WxValidate.addMethod('assistance', (value, param) => {
    return this.WxValidate.optional(value) || (value.length >= 1 && value.length <= 2)
  }, '请勾选1-2个敲码助手')


  // 如果有个表单字段的 assistance，则在 rules 中写

  // 调用验证方法，传入参数 e 是 form 表单组件中的数据
  },
    assistance: {
      required: true,
      assistance: true,
    },
  submitForm(e) {
    const params = e.detail.value
    // console.log(params)
  
    // 传入表单数据，调用验证方法
    if (!this.WxValidate.checkForm(params)) {
        // const error = this.WxValidate.errorList[0]
        const error = this.WxValidate.errorList
        wx.showToast({
          title: 'Submission Failed',
          icon: 'none',
          duration: 2000
        })
        // this.setData({
        //   username:error[0].msg,
        //   password:error[1].msg
        // });

        // console.log(error)
        return false
    }else{
      let registered_msg = e.detail.value;
      console.log(registered_msg)
      // let password = e.detail.value.password;
      // console.log(password)
      var that = this;
      var Util = require('../../utils/util.js');
      wx.request({ 
        url: 'https://jsonplaceholder.typicode.com/todos', 
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: "POST",
        data: registered_msg,
        
        success: function (res) { 
          // that.setData({ 
          //   registered_msg: registered_msg
          // }); 
          // console.log(res.data);
          wx.showToast({
            title: '',
            icon: 'success',
            duration: 2000
          })
        },
        fail(){  
          console.log('failed');
        },
        complete(){   
            console.log('complete'); 
        }
      })

      return true
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})